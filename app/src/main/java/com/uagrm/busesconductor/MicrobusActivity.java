package com.uagrm.busesconductor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.marcoscg.materialtoast.MaterialToast;
import com.uagrm.busesconductor.dialogo.Animacion;
import com.uagrm.busesconductor.dialogo.DialogoNormal;
import com.uagrm.busesconductor.dialogo.DialogoNormalInterface;
import com.uagrm.busesconductor.pojo.RegisterMicrobus;
import com.uagrm.busesconductor.pojo.ResponseRegister;
import com.uagrm.busesconductor.retrofit.Cliente;
import com.uagrm.busesconductor.retrofit.HomeBeautyService;
import com.uagrm.busesconductor.retrofit.Respuesta;
import com.uagrm.busesconductor.util.Preferencia;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MicrobusActivity extends AppCompatActivity {
    private List<String> lineas;
    private List<String> lineasId;
    private Typeface  typeFaceLight;
    FirebaseFirestore db ;
    private String imageBase64;
    private Button btn_crear_cuenta_subir_foto,btn_crear_microbus_guardar;
    private Typeface typefaceBold;
    private MaterialSpinner ms_crear_cuenta_linea;
    private String displayName = null;
    private SweetAlertDialog sweetAlertDialog;
    private RegisterMicrobus registerMicrobus;
    private Preferencia preferencia;
    String lineaSeleccionada ;
    private Boolean imageselect = false;
    private CircleImageView civ_crear_cuenta_foto;
    private Uri imageUri;
    private EditText et_placa,et_modelo,et_cantidad_asientos,
            et_numero_interno, et_fecha_asignacion, et_fecha_baja;
    private static final int PHOTO_SELECTED = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_microbus);
        typeFaceLight = Typeface.createFromAsset(getAssets(), "fonts/oxygen_light.ttf");
        lineas = new ArrayList<>();
        lineaSeleccionada= "Seleccione";
        db = FirebaseFirestore.getInstance();
        registerMicrobus = new RegisterMicrobus();
        lineasId = new ArrayList<>();
        civ_crear_cuenta_foto = findViewById(R.id.civ_crear_cuenta_foto);
        lineas.add("Linea 10");
        lineas.add("Linea 8");
        lineas.add("Linea 5");
        lineas.add("Linea 2");
        lineas.add("Linea 16");
        lineas.add("Linea 17");
        lineas.add("Linea 18");
        lineas.add("Linea 9");
        lineas.add("Linea 1");
        lineas.add("Linea 11");
        preferencia = new Preferencia(this);
        lineasId.add("0yTLatSYP76HqIJnuQ5y");
        typefaceBold = Typeface.createFromAsset(getAssets(), "fonts/oxygen_bold.ttf");
        lineasId.add("DlfLVLSClqCKblS8UShQ");
        lineasId.add("K9gavPZdN7ue7Qmqlnf9");
        lineasId.add("S219TiSztstSOccVUYyN");
        lineasId.add("iHiv6d3uD3TenfIHy4G");
        lineasId.add("pQjh4Is1SjgCCpfKffKD");
        lineasId.add("q2gQJxJMDtCinAWVqyWJ");
        lineasId.add("uUrNvRKE8gpuCLh0oKgS");
        lineasId.add("wWCKPapryLtnEQcswwFR");
        lineasId.add("yMIVdzJLi5SW56ITMb5L");
        btn_crear_cuenta_subir_foto = findViewById(R.id.btn_crear_cuenta_subir_foto);
        btn_crear_cuenta_subir_foto.setTypeface(typefaceBold);
        btn_crear_cuenta_subir_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimary));
        ms_crear_cuenta_linea = findViewById(R.id.ms_crear_cuenta_linea);
        ms_crear_cuenta_linea.setTypeface(typeFaceLight);
        ms_crear_cuenta_linea.setItems(lineas);
        ms_crear_cuenta_linea.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                registerMicrobus.setLinea_id(lineasId.get(position));
                lineaSeleccionada = lineasId.get(position);
                preferencia.setLinea(lineas.get(position));
                preferencia.setLineaId(lineasId.get(position));
            }
        });
        et_placa = findViewById(R.id.et_placa);
        et_placa.setTypeface(typeFaceLight);
        et_modelo = findViewById(R.id.et_modelo);
        et_modelo.setTypeface(typeFaceLight);
        et_cantidad_asientos = findViewById(R.id.et_cantidad_asientos);
        et_cantidad_asientos.setTypeface(typeFaceLight);
        et_numero_interno = findViewById(R.id.et_numero_interno);
        et_numero_interno.setTypeface(typeFaceLight);

        et_fecha_asignacion = findViewById(R.id.et_fecha_asignacion);
        et_fecha_asignacion.setTypeface(typeFaceLight);
        et_fecha_baja = findViewById(R.id.et_fecha_baja);
        et_fecha_baja.setTypeface(typeFaceLight);
        btn_crear_microbus_guardar = findViewById(R.id.btn_crear_microbus_guardar);
        btn_crear_microbus_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!verificarDatosBlanco()) {
                sweetAlertDialog.setTitleText("Cargando...");
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.show();

                registerMicrobus.setPlaca(et_placa.getText().toString());
                registerMicrobus.setModelo(et_modelo.getText().toString() );
                registerMicrobus.setCantidad_asientos(Integer.valueOf(et_cantidad_asientos.getText().toString()));
                registerMicrobus.setConductor_id(preferencia.getConductorId());
                registerMicrobus.setNumero_interno(et_numero_interno.getText().toString());
                registerMicrobus.setFoto(imageBase64);
                registerMicrobus.setFecha_baja(et_fecha_baja.getText().toString());
                registerMicrobus.setFecha_asignacion(et_fecha_asignacion.getText().toString());
                    sweetAlertDialog.setTitle("Registrando cuenta");
                    sweetAlertDialog.setTitleText("Cargando...");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.show();
                    db.collection("Microbuses")
                            .add(registerMicrobus)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    sweetAlertDialog.dismiss();
                                    Intent i = new Intent(MicrobusActivity.this,MenuActivity.class);
                                    startActivity(i);
                                    finish();
                                    preferencia.setFotoPerfil(registerMicrobus.getFoto());
                                    preferencia.setLogeado(true);
                                    abrirDialogoError("El registro se ha completado", "registro completado exitosamente.");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    abrirDialogoError("Lo sentimos, ha ocurrido un error", "Inténtelo más tarde, por favor.");
                                    sweetAlertDialog.dismiss();
                                }
                            });
                /*HomeBeautyService service = Cliente.getClient();
                Call<Respuesta<ResponseRegister>> call = service.registerMicrobus(registerMicrobus);
                call.enqueue(new Callback<Respuesta<ResponseRegister>>() {
                    @Override
                    public void onResponse(Call<Respuesta<ResponseRegister>> call, Response<Respuesta<ResponseRegister>> response) {
                        if (response.isSuccessful()) {
                            try{
                                final Respuesta<ResponseRegister> respuesta = response.body();
                                if (respuesta.respuestaExitosa()) {
                                    sweetAlertDialog.dismiss();
                                    Intent i = new Intent(MicrobusActivity.this,MenuActivity.class);
                                    startActivity(i);
                                    finish();
                                    preferencia.setLogeado(true);
                                }else{
                                    abrirDialogoError("Lo sentimos, ha ocurrido un error",respuesta.getMensaje());
                                    sweetAlertDialog.dismiss();
                                }
                            } catch (Exception e) {
                                abrirDialogoError("Lo sentimos, ha ocurrido un error",e.getMessage());
                                sweetAlertDialog.dismiss();
                            }
                        }else{
                            abrirDialogoError("Lo sentimos, ha ocurrido un error","Inténtelo más tarde, por favor.");
                            sweetAlertDialog.dismiss();
                        }

                    }

                    @Override
                    public void onFailure(Call<Respuesta<ResponseRegister>> call, Throwable t) {
                        Log.d("Excepcion", t.getMessage());
                    }
                });*/

            }
            }
        });

    }

    private void abrirDialogoError(String titulo,String mensaje) {
        new DialogoNormal.Builder(MicrobusActivity.this)
                .setTitle(titulo)
                .setMessage(mensaje)
                .setPositiveBtnText("Aceptar")
                .setAnimation(Animacion.POP)
                .isCancellable(false)
                .OnPositiveClicked(new DialogoNormalInterface() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }
    private void openGallery(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PHOTO_SELECTED);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imageBase64 = "";

        if (requestCode == PHOTO_SELECTED) {
            if (resultCode == MicrobusActivity.RESULT_OK) {


                imageUri = data.getData();
                civ_crear_cuenta_foto.setImageURI(imageUri);

                Uri uri = data.getData();
                String uriString = uri.toString();
                File myFile = new File(uriString);
                String path = myFile.getAbsolutePath();


                if (uriString.startsWith("content://")) {
                    Cursor cursor = null;
                    try {
                        cursor = getApplication().getContentResolver().query(uri, null, null, null, null);
                        if (cursor != null && cursor.moveToFirst()) {
                            displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        }
                    } finally {
                        cursor.close();
                    }
                } else if (uriString.startsWith("file://")) {
                    displayName = myFile.getName();
                }

                Bitmap bitmap = null;
                try {
                    imageselect = true;
                    bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), imageUri);
                } catch (IOException e) {
                    imageselect = false;
                    e.printStackTrace();
                }
                Bitmap image = ((BitmapDrawable) this.civ_crear_cuenta_foto.getDrawable()).getBitmap();
                Bitmap imageScaled = Bitmap.createScaledBitmap(image, 400, 400, false);


                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                imageScaled.compress(Bitmap.CompressFormat.PNG, 80, outputStream);
                byte[] byteArray = outputStream.toByteArray();

                imageBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                displayName = displayName.substring(0,displayName.length()-3)+"png";



            }
        }
    }


    private boolean verificarDatosBlanco() {
        if (et_placa.getText().toString().trim().equals("")) {
            MaterialToast.makeText(this, "Ingresar su placa", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        } else if (et_modelo.getText().toString().trim().equals("")) {
            MaterialToast.makeText(this, "Ingresar su modelo", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        }else if (et_cantidad_asientos.getText().toString().trim().equals("")) {
            MaterialToast.makeText(this, "Ingresar la cantidad de asientos", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        }else if (lineaSeleccionada == "Seleccione") {
            MaterialToast.makeText(this, "Seleccione una linea", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }

    }
}