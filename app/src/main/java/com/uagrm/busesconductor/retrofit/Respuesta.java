package com.uagrm.busesconductor.retrofit;


public class Respuesta<T> {

    public String Codigo;
    public String FechaHora;
    public String Mensaje;
    public T Data;


    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getFechaHora() {
        return FechaHora;
    }

    public void setFechaHora(String fechaHora) {
        FechaHora = fechaHora;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public T getData() {
        return Data;
    }

    public void setData(T data) {
        Data = data;
    }

    public boolean respuestaExitosa() {

        if (Codigo.equals("0")) {
            return true;
        } else if (Codigo.equals("1")) {
            return false;
        }
        return true;
    }
}
