package com.uagrm.busesconductor.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class Preferencia {
    private SharedPreferences sharedPreferences;

    public Preferencia(Context context) {
        sharedPreferences = context.getSharedPreferences("datosPersonales", MODE_PRIVATE);
    }

    public void setMostrarPresentacion(boolean b) {
        sharedPreferences.edit().putBoolean("mostrarPresentacion", b).apply();

    }

    public Boolean getMostrarPresentacion() {
        return sharedPreferences.getBoolean("mostrarPresentacion", true);
    }

    public void setPermiso(boolean b) {
        sharedPreferences.edit().putBoolean("permisos", b).apply();

    }

    public Boolean getPermiso() {
        return sharedPreferences.getBoolean("permisos", false);
    }



    public void setLogeado(boolean b) {
        sharedPreferences.edit().putBoolean("logeado", b).apply();
    }

    public Boolean getLogeado() {
        return sharedPreferences.getBoolean("logeado", false);
    }

    public void setCarreraInicio(boolean b) {
        sharedPreferences.edit().putBoolean("carreraInicio", b).apply();
    }

    public Boolean getCarreraInicio() {
        return sharedPreferences.getBoolean("carreraInicio", false);
    }

    public void setLoginFacebook(boolean b) {
        sharedPreferences.edit().putBoolean("loginfacebook", b).apply();
    }

    public Boolean getLoginFacebook() {
        return sharedPreferences.getBoolean("loginfacebook", false);
    }


    public void setLoginGmail(boolean b) {
        sharedPreferences.edit().putBoolean("logingmail", b).apply();
    }

    public Boolean getLoginGmail() {
        return sharedPreferences.getBoolean("logingmail", false);
    }


    public void setLoginNormal(boolean b) {
        sharedPreferences.edit().putBoolean("loginnormal", b).apply();
    }

    public Boolean getLoginNormal() {
        return sharedPreferences.getBoolean("loginnormal", false);
    }


    public void setNombreCompleto(String b) {
        sharedPreferences.edit().putString("nombrecompleto", b).apply();

    }

    public String getToken() {
        return sharedPreferences.getString("tokenpassword", "");
    }


    public void setToken(String b) {
        sharedPreferences.edit().putString("tokenpassword", b).apply();

    }

    public String getPassword() {
        return sharedPreferences.getString("passwordd", "");
    }


    public void setPassword(String b) {
        sharedPreferences.edit().putString("passwordd", b).apply();

    }
    public String getNombreCompleto() {
        return sharedPreferences.getString("nombrecompleto", "");
    }


    public void setNombre(String b) {
        sharedPreferences.edit().putString("nombre", b).apply();

    }

    public String getNombre() {
        return sharedPreferences.getString("nombre", "");
    }

    public void setConductorId(String id) {
        sharedPreferences.edit().putString("conductor_id", id).apply();

    }

    public String getConductorId() {
        return sharedPreferences.getString("conductor_id", "0");
    }
    public void setApellido(String b) {
        sharedPreferences.edit().putString("apellidos", b).apply();

    }

    public String getNit() {
        return sharedPreferences.getString("nit", "");
    }


    public void setNit(String b) {
        sharedPreferences.edit().putString("nit", b).apply();

    }

    public String getLinea() {
        return sharedPreferences.getString("linea", "");
    }


    public void setLinea(String b) {
        sharedPreferences.edit().putString("linea", b).apply();

    }
    public String getCarreraId() {
        return sharedPreferences.getString("carrera_id", "");
    }


    public void setCarreraId(String b) {
        sharedPreferences.edit().putString("carrera_id", b).apply();

    }

    public String getLineaId() {
        return sharedPreferences.getString("linea_id", "");
    }


    public void setLineaId(String b) {
        sharedPreferences.edit().putString("linea_id", b).apply();

    }

    public String getRazonSocial() {
        return sharedPreferences.getString("razonsocial", "");
    }


    public void setRazonSocial(String b) {
        sharedPreferences.edit().putString("razonsocial", b).apply();

    }

    public String getApellido() {
        return sharedPreferences.getString("apellidos", "");
    }


    public void setUidFacebook(String b) {
        sharedPreferences.edit().putString("uidfacebook", b).apply();

    }

    public String getUidFacebook() {
        return sharedPreferences.getString("uidfacebook", "");
    }

    public void setUidGoogle(String b) {
        sharedPreferences.edit().putString("uidgoogle", b).apply();

    }

    public String getUidGoogle() {
        return sharedPreferences.getString("uidgoogle", "");
    }



    public void setTelefono(String b) {
        sharedPreferences.edit().putString("telefono", b).apply();

    }

    public String getTelefono() {
        return sharedPreferences.getString("telefono", "");
    }



    public void setEmail(String b) {
        sharedPreferences.edit().putString("email", b).apply();

    }

    public String getEmail() {
        return sharedPreferences.getString("email", "");
    }



    public void setUsuarioId(int b) {
        sharedPreferences.edit().putInt("usuarioid", b).apply();
    }

    public int getUsuarioId() {
        return sharedPreferences.getInt("usuarioid", 0);
    }


    public void setFotoPerfil(String b) {
        sharedPreferences.edit().putString("fotoperfil", b).apply();
    }

    public String getFotoPerfil() {
        return sharedPreferences.getString("fotoperfil", "");
    }



    public void setGeneroId(int b) {
        sharedPreferences.edit().putInt("generoid", b).apply();
    }

    public int getGeneroId() {
        return sharedPreferences.getInt("generoid", -1);
    }



    public void setNombreCiudad(String b) {
        sharedPreferences.edit().putString("nombreciudad", b).apply();
    }
    public String getNombreCiudad() {
        return sharedPreferences.getString("nombreciudad", "");
    }


    public String getNombreUsuario() {
        return sharedPreferences.getString("nombreusuario", "");
    }

    public void setNombreUsuario(String b) {
        sharedPreferences.edit().putString("nombreusuario", b).apply();
    }




    public void setCiudadId(int b) {
        sharedPreferences.edit().putInt("ciudadid", b).apply();
    }

    public int getCiudadId() {
        return sharedPreferences.getInt("ciudadid", -1);
    }



    public void setLatitudOrigen(String b) {
        sharedPreferences.edit().putString("latitudorigen", b).apply();
    }

    public String getLatitudOrigen() {
        return sharedPreferences.getString("latitudorigen", "");
    }



    public void setLongitudOrigen(String b) {
        sharedPreferences.edit().putString("longitudorigen", b).apply();
    }

    public String getLongitudOrigen() {
        return sharedPreferences.getString("longitudorigen", "");
    }

    public void setDireccion(String b) {
        sharedPreferences.edit().putString("direccionpedido", b).apply();
    }

    public String getDireccion() {
        return sharedPreferences.getString("direccionpedido", "");
    }



    public void clearDatosLogin() {
        sharedPreferences.edit().putString("nombrecompleto", "").apply();
        sharedPreferences.edit().putString("nombre", "").apply();
        sharedPreferences.edit().putString("apellidos", "").apply();
        sharedPreferences.edit().putString("telefono", "").apply();
        sharedPreferences.edit().putString("email", "").apply();
        sharedPreferences.edit().putInt("usuarioid", 0).apply();
        sharedPreferences.edit().putString("fotoperfil", "").apply();
        sharedPreferences.edit().putString("uid", "").apply();
        sharedPreferences.edit().putString("nombreusuario", "").apply();
        sharedPreferences.edit().putInt("generoid", 0).apply();
        sharedPreferences.edit().putString("nit", "").apply();
        sharedPreferences.edit().putString("razonsocial", "").apply();
        sharedPreferences.edit().putString("nombreciudad", "").apply();
        sharedPreferences.edit().putInt("generoid", -1).apply();
        sharedPreferences.edit().putInt("ciudadid", -1).apply();



    }

}
