package com.uagrm.busesconductor.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;


public class Permisos {
    private Activity activity;

    //PERMISOS

    public Permisos(Activity activity) {
        this.activity = activity;
    }

    public static int getMultiplePermissionsRequestCode() {
        return MULTIPLE_PERMISSIONS_REQUEST_CODE;
    }

    public static final int MULTIPLE_PERMISSIONS_REQUEST_CODE = 7;
    private String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.VIBRATE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE,};



    public void solicitarPermisos() {
        if (ActivityCompat.checkSelfPermission(activity, permissions[0]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, permissions[1]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, permissions[2]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, permissions[3]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, permissions[4]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, permissions[5]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, permissions[6]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, permissions[7]) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity, permissions, MULTIPLE_PERMISSIONS_REQUEST_CODE);
        }else {
            Preferencia preferencia = new Preferencia(activity);
            preferencia.setPermiso(true);
            //Si todos los permisos estan concedidos prosigue con el flujo normal
        }
    }


    public boolean validatePermissions(int[] grantResults) {
        boolean allGranted = false;
        //Revisa cada uno de los permisos y si estos fueron aceptados o no
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                //Si todos los permisos fueron aceptados retorna true
                Preferencia preferencia = new Preferencia(activity);
                preferencia.setPermiso(true);
                allGranted = true;
            } else {
                //Si algun permiso no fue aceptado retorna false
                allGranted = false;
                break;
            }
        }
        return allGranted;
    }
}
