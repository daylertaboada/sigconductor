package com.uagrm.busesconductor.pojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class Ruta implements Serializable {
    private double Shape_Len;
    private double distancia;
    private String oneway;
    private double tiempo;
    private HashMap<String, Punto> coordenadas;



    public double getShape_Len() {
        return Shape_Len;
    }

    public void setShape_Len(double shape_Len) {
        Shape_Len = shape_Len;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public String getOneway() {
        return oneway;
    }

    public void setOneway(String oneway) {
        this.oneway = oneway;
    }

    public double getTiempo() {
        return tiempo;
    }

    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }

    public HashMap<String, Punto> getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(HashMap<String, Punto> coordenadas) {
        this.coordenadas = coordenadas;
    }
}
