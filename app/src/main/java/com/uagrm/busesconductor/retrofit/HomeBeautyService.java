package com.uagrm.busesconductor.retrofit;

import com.uagrm.busesconductor.pojo.RegisterConductor;
import com.uagrm.busesconductor.pojo.RegisterMicrobus;
import com.uagrm.busesconductor.pojo.RequestLogin;
import com.uagrm.busesconductor.pojo.ResponseRegister;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface HomeBeautyService {

    /*@GET("ciudad/ciudades")
    Call<Respuesta<List<CiudadJson>>> listarCiudades();

    @POST("usuario/login")
    Call<Respuesta<LoginJson>> login(@Body LoginPojo loginPojo);*/

    @POST("sig/registrar-conductor")
    Call<Respuesta<ResponseRegister>> registerConductor(@Body RegisterConductor registerConductor);
    @POST("sig/registrar-microbus")
    Call<Respuesta<ResponseRegister>> registerMicrobus(@Body RegisterMicrobus registerMicrobus);
    @POST("sig/iniciar-sesion")
    Call<Respuesta<ResponseRegister>> loginConductor(@Body RequestLogin requestLogin);
}
