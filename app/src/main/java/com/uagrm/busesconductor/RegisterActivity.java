package com.uagrm.busesconductor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.marcoscg.materialtoast.MaterialToast;
import com.uagrm.busesconductor.dialogo.Animacion;
import com.uagrm.busesconductor.dialogo.DialogoNormal;
import com.uagrm.busesconductor.dialogo.DialogoNormalInterface;
import com.uagrm.busesconductor.pojo.RegisterConductor;
import com.uagrm.busesconductor.pojo.ResponseRegister;
import com.uagrm.busesconductor.retrofit.Cliente;
import com.uagrm.busesconductor.retrofit.HomeBeautyService;
import com.uagrm.busesconductor.retrofit.Respuesta;
import com.uagrm.busesconductor.util.Preferencia;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private List<String> generos;
    private List<Integer> generosId;
    private List<String> tipoLicencias;
    private List<Integer> tipoLicenciasId;
    FirebaseFirestore db ;
    private static final int PHOTO_SELECTED = 100;
    private Typeface typefaceBold, typeFaceLight;
    private MaterialSpinner ms_crear_tipo_licencia,ms_crear_cuenta_genero;
    private RegisterConductor registerConductor;
    private Button btn_crear_cuenta_guardar;
    int sexoSeleccionado ;
    int tipoLicenciaSeleccionado ;
    private SweetAlertDialog sweetAlertDialog;
    private Preferencia preferencia;
    private EditText et_nombres,et_apelidos,et_ci,et_telefono, et_email,et_fecha_nacimiento,et_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        typeFaceLight = Typeface.createFromAsset(getAssets(), "fonts/oxygen_light.ttf");
        registerConductor = new RegisterConductor();
         db = FirebaseFirestore.getInstance();
        generos = new ArrayList<>();
        generosId = new ArrayList<>();
        generos.add("Seleccione su genero");
        generos.add("Masculino");
        generos.add("Femenino");
        generosId.add(-1);
        generosId.add(1);
        generosId.add(2);
        preferencia = new Preferencia(this);
        sexoSeleccionado= -1;
        tipoLicenciaSeleccionado=-1;
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimary));
        ms_crear_cuenta_genero = findViewById(R.id.ms_crear_cuenta_genero);
        ms_crear_cuenta_genero.setTypeface(typeFaceLight);
        ms_crear_cuenta_genero.setItems(generos);

        tipoLicencias = new ArrayList<>();
        tipoLicenciasId = new ArrayList<>();
        tipoLicencias.add("Seleccione su tipo de licencia");
        tipoLicencias.add("A");
        tipoLicencias.add("B");
        tipoLicenciasId.add(-1);
        tipoLicenciasId.add(1);
        tipoLicenciasId.add(2);
        ms_crear_cuenta_genero.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                registerConductor.setSexo(generos.get(position));
                sexoSeleccionado = generosId.get(position);
            }
        });

        ms_crear_tipo_licencia = findViewById(R.id.ms_crear_cuenta_tipo_licencia);
        ms_crear_tipo_licencia.setTypeface(typeFaceLight);
        ms_crear_tipo_licencia.setItems(tipoLicencias);
        ms_crear_tipo_licencia.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                registerConductor.setCategoria_licencia(tipoLicencias.get(position));
                tipoLicenciaSeleccionado = tipoLicenciasId.get(position);
            }
        });
        btn_crear_cuenta_guardar = findViewById(R.id.btn_crear_cuenta_guardar);

        et_nombres = findViewById(R.id.et_nombres);
        et_nombres.setTypeface(typeFaceLight);
        et_apelidos = findViewById(R.id.et_apelidos);
        et_apelidos.setTypeface(typeFaceLight);
        et_ci = findViewById(R.id.et_ci);
        et_ci.setTypeface(typeFaceLight);
        et_telefono = findViewById(R.id.et_telefono);
        et_telefono.setTypeface(typeFaceLight);
        et_email = findViewById(R.id.et_email);
        et_email.setTypeface(typeFaceLight);
        et_fecha_nacimiento = findViewById(R.id.et_fecha_nacimiento);
        et_fecha_nacimiento.setTypeface(typeFaceLight);
        et_password = findViewById(R.id.et_password);
        et_password.setTypeface(typeFaceLight);
        db.collection("Lineas")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("Respuesta Correcta", document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.w("Respuesta Correcta", "Error getting documents.", task.getException());
                        }
                    }
                });
        btn_crear_cuenta_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                registerConductor.setNombres(et_nombres.getText().toString());
                registerConductor.setApellidos(et_apelidos.getText().toString() );
                registerConductor.setCi(et_ci.getText().toString());
                registerConductor.setEmail(et_email.getText().toString());
                registerConductor.setTelefono(et_telefono.getText().toString());
                registerConductor.setFecha_nacimiento(et_fecha_nacimiento.getText().toString());
                registerConductor.setPassword(et_password.getText().toString());

                if (!verificarDatosBlanco()) {
                    sweetAlertDialog.setTitle("Registrando cuenta");
                    sweetAlertDialog.setTitleText("Cargando...");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.show();
                    db.collection("Conductores")
                            .add(registerConductor)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    sweetAlertDialog.dismiss();
                                    preferencia.setConductorId(documentReference.getId());
                                    preferencia.setNombre(registerConductor.getNombres() +" "+ registerConductor.getApellidos());
                                    Intent i = new Intent(RegisterActivity.this, MicrobusActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    abrirDialogoError("Lo sentimos, ha ocurrido un error", "Inténtelo más tarde, por favor.");
                                    sweetAlertDialog.dismiss();
                                }
                            });
                    /*sweetAlertDialog.setTitle("Registrando cuenta");
                    sweetAlertDialog.setTitleText("Cargando...");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.show();
                    HomeBeautyService service = Cliente.getClient();
                    Call<Respuesta<ResponseRegister>> call = service.registerConductor(registerConductor);
                    call.enqueue(new Callback<Respuesta<ResponseRegister>>() {
                        @Override
                        public void onResponse(Call<Respuesta<ResponseRegister>> call, Response<Respuesta<ResponseRegister>> response) {
                            if (response.isSuccessful()) {
                                try {
                                    final Respuesta<ResponseRegister> respuesta = response.body();
                                    if (respuesta.respuestaExitosa()) {
                                        sweetAlertDialog.dismiss();
                                        preferencia.setConductorId(respuesta.Data.getId());
                                        Intent i = new Intent(RegisterActivity.this, MicrobusActivity.class);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        abrirDialogoError("Lo sentimos, ha ocurrido un error", respuesta.getMensaje());
                                        sweetAlertDialog.dismiss();
                                    }
                                } catch (Exception e) {
                                    abrirDialogoError("Lo sentimos, ha ocurrido un error", e.getMessage());
                                    sweetAlertDialog.dismiss();
                                }
                            } else {
                                abrirDialogoError("Lo sentimos, ha ocurrido un error", "Inténtelo más tarde, por favor.");
                                sweetAlertDialog.dismiss();
                            }

                        }

                        @Override
                        public void onFailure(Call<Respuesta<ResponseRegister>> call, Throwable t) {
                            Log.d("Excepcion", t.getMessage());
                        }
                    });*/
                }else{
                    sweetAlertDialog.dismiss();
                }

            }
        });
    }
    private void abrirDialogoError(String titulo,String mensaje) {
        new DialogoNormal.Builder(RegisterActivity.this)
                .setTitle(titulo)
                .setMessage(mensaje)
                .setPositiveBtnText("Aceptar")
                .setAnimation(Animacion.POP)
                .isCancellable(false)
                .OnPositiveClicked(new DialogoNormalInterface() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }

    private boolean verificarDatosBlanco() {
        if (et_nombres.getText().toString().trim().equals("")) {
            MaterialToast.makeText(this, "Ingresar su(s) nombres(s)", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        } else if (et_apelidos.getText().toString().trim().equals("")) {
            MaterialToast.makeText(this, "Ingresar su(s) apellidos(s)", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        }else if (et_ci.getText().toString().trim().equals("")) {
            MaterialToast.makeText(this, "Ingresar su ci", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        }else if (et_email.getText().toString().trim().equals("")) {
            MaterialToast.makeText(this, "Ingresar su email", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        } else if (sexoSeleccionado ==-1) {
            MaterialToast.makeText(this, "Seleccione un genero", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        }else if (tipoLicenciaSeleccionado == -1) {
            MaterialToast.makeText(this, "Seleccione un tipo de licencia", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }

    }

}