package com.uagrm.busesconductor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.marcoscg.materialtoast.MaterialToast;
import com.uagrm.busesconductor.dialogo.Animacion;
import com.uagrm.busesconductor.dialogo.DialogoNormal;
import com.uagrm.busesconductor.dialogo.DialogoNormalInterface;
import com.uagrm.busesconductor.pojo.Conductor;
import com.uagrm.busesconductor.pojo.Linea;
import com.uagrm.busesconductor.pojo.Punto;
import com.uagrm.busesconductor.pojo.RegisterConductor;
import com.uagrm.busesconductor.pojo.RegisterMicrobus;
import com.uagrm.busesconductor.pojo.RequestLogin;
import com.uagrm.busesconductor.pojo.ResponseRegister;
import com.uagrm.busesconductor.retrofit.Cliente;
import com.uagrm.busesconductor.retrofit.HomeBeautyService;
import com.uagrm.busesconductor.retrofit.Respuesta;
import com.uagrm.busesconductor.util.Preferencia;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;

public class LoginActivity extends AppCompatActivity {
    private Button btn_login_ingresar;
    private Button btn_register;
    private Preferencia preferencia;
    FirebaseFirestore db ;
    FirebaseFirestore dbCon ;
    private SweetAlertDialog sweetAlertDialog;
    private EditText et_email,et_password;
    DocumentReference docRef;
    private Typeface typefaceBold, typeFaceLight;
    private RequestLogin requestLogin;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        et_password = findViewById(R.id.et_password);
        et_password.setTypeface(typeFaceLight);
        et_email = findViewById(R.id.et_email);
        et_email.setTypeface(typeFaceLight);
        preferencia = new Preferencia(this);
        requestLogin = new RequestLogin();
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimary));
        db = FirebaseFirestore.getInstance();
        dbCon = FirebaseFirestore.getInstance();
        if(preferencia.getLogeado()){
            Intent i = new Intent(LoginActivity.this, MenuActivity.class);
            startActivity(i);
            finish();
        }
        iniciar();


    }

    private void iniciar() {
        btn_login_ingresar = findViewById(R.id.btn_login_ingresar);
        btn_login_ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sweetAlertDialog.setTitle("Verificando Datos");
                sweetAlertDialog.setTitleText("Cargando...");
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.show();
                requestLogin.setEmail(et_email.getText().toString());
                requestLogin.setPassword(et_password.getText().toString());
                HomeBeautyService service = Cliente.getClient();
                Call<Respuesta<ResponseRegister>> call = service.loginConductor(requestLogin);

                if (!verificarDatosBlanco()) {
                    db.collection("Conductores")
                            .whereEqualTo("email", requestLogin.getEmail())
                            .whereEqualTo("password",  requestLogin.getPassword())
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (task.isSuccessful()) {

                                        for (QueryDocumentSnapshot document : task.getResult()) {

                                            Conductor conductor = document.toObject(Conductor.class);
                                            preferencia.setNombre(conductor.getNombres() +" "+ conductor.getApellidos());
                                            Log.d("Respuesta Correcta", document.getId() + " => " + conductor.getApellidos());
                                            dbCon.collection("Microbuses")
                                                    .whereEqualTo("conductor_id", document.getId())
                                                    .get()
                                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                            if (task.isSuccessful()) {

                                                                for (QueryDocumentSnapshot document : task.getResult()) {

                                                                    RegisterMicrobus registerMicrobus = document.toObject(RegisterMicrobus.class);


                                                                    preferencia.setLineaId(registerMicrobus.getLinea_id());
                                                                    preferencia.setFotoPerfil(registerMicrobus.getFoto());

                                                                    Log.d("Micro", preferencia.getFotoPerfil());
                                                                    sweetAlertDialog.dismiss();
                                                                }
                                                                abrirDialogoError("Datos incorrectos", "El correo o contraseña no son correctos");
                                                                sweetAlertDialog.dismiss();
                                                            } else {
                                                                abrirDialogoError("Error en el servidor", "Error no hay respuesta con el servidor");
                                                                sweetAlertDialog.dismiss();
                                                            }
                                                        }
                                                    });
                                            preferencia.setConductorId(document.getId());
                                            preferencia.setLogeado(true);
                                            Intent i = new Intent(LoginActivity.this, MenuActivity.class);
                                            startActivity(i);
                                            finish();
                                            sweetAlertDialog.dismiss();
                                        }
                                        abrirDialogoError("Datos incorrectos", "El correo o contraseña no son correctos");
                                        sweetAlertDialog.dismiss();
                                    } else {
                                        abrirDialogoError("Error en el servidor", "Error no hay respuesta con el servidor");
                                        sweetAlertDialog.dismiss();
                                    }
                                }
                            });
                }else{
                    sweetAlertDialog.dismiss();
                }

                /*call.enqueue(new Callback<Respuesta<ResponseRegister>>() {
                    @Override
                    public void onResponse(Call<Respuesta<ResponseRegister>> call, Response<Respuesta<ResponseRegister>> response) {
                        if (response.isSuccessful()) {
                            try {
                                final Respuesta<ResponseRegister> respuesta = response.body();
                                if (respuesta.respuestaExitosa()) {
                                    sweetAlertDialog.dismiss();
                                    preferencia.setConductorId(respuesta.Data.getId());
                                    Intent i = new Intent(LoginActivity.this, MenuActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    abrirDialogoError("Lo sentimos, ha ocurrido un error", respuesta.getMensaje());
                                    sweetAlertDialog.dismiss();
                                }
                            } catch (Exception e) {
                                abrirDialogoError("Lo sentimos, ha ocurrido un error", e.getMessage());
                                sweetAlertDialog.dismiss();
                            }
                        } else {
                            abrirDialogoError("Lo sentimos, ha ocurrido un error", "Inténtelo más tarde, por favor.");
                            sweetAlertDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<Respuesta<ResponseRegister>> call, Throwable t) {

                    }
                });*/
                        /*Intent i = new Intent(LoginActivity.this,MapaActivity.class);
                startActivity(i);
                finish();*/
            }
        });
        btn_register = findViewById(R.id.btn_login_registrarme);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
    private boolean verificarDatosBlanco() {
        if (et_email.getText().toString().trim().equals("")) {
            MaterialToast.makeText(this, "Ingresar su email", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        } else if (et_password.getText().toString().trim().equals("")) {
            MaterialToast.makeText(this, "Ingresar su password", R.drawable.ic_logo_error, Toast.LENGTH_SHORT).show();
            return true;
        }else {
            return false;
        }

    }
    private void abrirDialogoError(String titulo,String mensaje) {
        new DialogoNormal.Builder(LoginActivity.this)
                .setTitle(titulo)
                .setMessage(mensaje)
                .setPositiveBtnText("Aceptar")
                .setAnimation(Animacion.POP)
                .isCancellable(false)
                .OnPositiveClicked(new DialogoNormalInterface() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }
}