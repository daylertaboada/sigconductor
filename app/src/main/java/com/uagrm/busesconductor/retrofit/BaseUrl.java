package com.uagrm.busesconductor.retrofit;


public class BaseUrl {

    //public static String baseUrlJS = "http://192.168.0.7:82/api/";

    public static String baseUrlJS = "http://178.238.236.77:8080/api/";

    public static String baseUrl = baseUrlJS;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
