package com.uagrm.busesconductor;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.uagrm.busesconductor.pojo.Linea;
import com.uagrm.busesconductor.pojo.Punto;
import com.uagrm.busesconductor.util.Preferencia;

import java.lang.reflect.Type;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MenuActivity extends AppCompatActivity {

    private Button btn_ir_al_mapa;
    FirebaseFirestore db;
    private Button btn_conductor;
    DocumentReference docRef;
    private Button btn_microbus,btn_cerrar_sesion;
    private TextView tv_punto_nombre_usuario,tv_punto_puntaje;
    private Preferencia preferencia;
    private CircleImageView circleImageView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        iniciar();
    }

    private void iniciar() {
        btn_ir_al_mapa = findViewById(R.id.btn_ir_al_mapa);
        tv_punto_nombre_usuario = findViewById(R.id.tv_punto_nombre_usuario);
        tv_punto_puntaje = findViewById(R.id.tv_punto_puntaje);
        btn_cerrar_sesion = findViewById(R.id.btn_cerrar_sesion);
        circleImageView = findViewById(R.id.civ_punto_usuario);
        preferencia = new Preferencia(this);
        db = FirebaseFirestore.getInstance();
        docRef = db.collection("Lineas").document(preferencia.getLineaId());
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Linea linea = documentSnapshot.toObject(Linea.class);
                tv_punto_puntaje.setText(linea.getName().toString());

            }
        });
        final String encodedString = preferencia.getFotoPerfil();
        final String pureBase64Encoded = encodedString.substring(encodedString.indexOf(",")  + 1);
        final byte[] decodedBytes = Base64.decode(pureBase64Encoded, Base64.DEFAULT);
        Glide.with(MenuActivity.this).load(decodedBytes).fitCenter().into(circleImageView);
        tv_punto_nombre_usuario.setText(preferencia.getNombre());
        btn_cerrar_sesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferencia.setLogeado(false);
                Intent i = new Intent(MenuActivity.this,LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        btn_ir_al_mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuActivity.this,MapaActivity.class);
                startActivity(i);
                finish();
            }
        });

        btn_conductor = findViewById(R.id.btn_login_registrarme);
        btn_conductor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuActivity.this,RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });
        btn_microbus = findViewById(R.id.btn_login_microbus);
        btn_microbus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuActivity.this,MicrobusActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}