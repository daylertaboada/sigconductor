package com.uagrm.busesconductor;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.uagrm.busesconductor.dialogo.Animacion;
import com.uagrm.busesconductor.dialogo.DialogoNormal;
import com.uagrm.busesconductor.dialogo.DialogoNormalInterface;
import com.uagrm.busesconductor.pojo.Carrera;
import com.uagrm.busesconductor.pojo.Linea;
import com.uagrm.busesconductor.pojo.Punto;
import com.uagrm.busesconductor.pojo.Recorrido;
import com.uagrm.busesconductor.pojo.Ruta;
import com.uagrm.busesconductor.util.Preferencia;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MapaActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private Button btn_iniciar, btn_finalizar;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int REQUEST_CHECK_SETTINGS = 102;
    private Preferencia preferencia;
    FirebaseFirestore db;
    List<Punto> puntos;
    Carrera carrera ;
    List<Punto> puntosVuelta;
    private SweetAlertDialog sweetAlertDialog;
    DocumentReference docRef;
    // La clase FusedLocationProviderClient
    private FusedLocationProviderClient fusedLocationClient;

    // La clase LocationCallback se utiliza para recibir notificaciones de FusedLocationProviderApi
    // cuando la ubicación del dispositivo ha cambiado o ya no se puede determinar.
    private LocationCallback mlocationCallback;

    // La clase LocationSettingsRequest.Builder extiende un Object
    // y construye una LocationSettingsRequest.
    private LocationSettingsRequest.Builder builder;

    // La clase LocationRequest sirve para  para solicitar las actualizaciones
    // de ubicación de FusedLocationProviderApi
    public LocationRequest mLocationRequest;
    public Chronometer cronometro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferencia = new Preferencia(this);
        preferencia.setCarreraInicio(false);
         carrera = new Carrera();
        db = FirebaseFirestore.getInstance();
        obtenerUltimaUbicacion();
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimary));


        setContentView(R.layout.activity_mapa_cliente);
         /*docRef = db.collection("Lineas").document("0yTLatSYP76HqIJnuQ5y");
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Linea linea = documentSnapshot.toObject(Linea.class);

                Log.d("RESPUESTA", "DocumentSnapshot data: " + linea.getLI().get("coordenadas"));

                Type listType = new TypeToken<ArrayList<Punto>>(){}.getType();
                puntos = new Gson().fromJson(linea.getLI().get("coordenadas").toString(), listType);
                Log.d("RESPUESTA", "DocumentSnapshot data: " + puntos.get(0).getLatitud());
            }
        });*/
        SupportMapFragment fragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);
        cronometro = findViewById(R.id.cronometro);
        btn_iniciar = findViewById(R.id.btn_iniciar);
        btn_iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!preferencia.getCarreraInicio()) {
                    sweetAlertDialog.setTitleText("Cargando...");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.show();
                    sweetAlertDialog.setTitle("Registrando cuenta");
                    sweetAlertDialog.setTitleText("Cargando...");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.show();


                    carrera.setConductor_id(preferencia.getConductorId());
                    carrera.setEstado("Iniciado");
                    Date date = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    carrera.setInicio(formatter.format(date));
                    carrera.setFin(" ");
                    db.collection("Carreras")
                            .add(carrera)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    sweetAlertDialog.dismiss();
                                    long systemCurrTime = SystemClock.elapsedRealtime();
                                    cronometro.setBase(systemCurrTime);
                                    cronometro.start();
                                    preferencia.setCarreraId(documentReference.getId());
                                    preferencia.setCarreraInicio(true);
                                    abrirDialogoError("Carrera Iniciada", "La carrera se ha iniciado exitosamente.");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    abrirDialogoError("Lo sentimos, ha ocurrido un error", "Inténtelo más tarde, por favor.");
                                    sweetAlertDialog.dismiss();
                                }
                            });
                    ejecutar();
                }else{
                    abrirDialogoError("La carrera no se finalizó", "Hay una carrera en curso.");
                }
            }
        });
        btn_finalizar = findViewById(R.id.btn_finalizar);
        btn_finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(preferencia.getCarreraInicio()){


                long elapsedMillis = SystemClock.elapsedRealtime() - cronometro.getBase();
                Toast.makeText(MapaActivity.this, "Milisegundos: " + elapsedMillis, Toast.LENGTH_LONG).show();
                cronometro.stop();
                sweetAlertDialog.setTitleText("Cargando...");
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.show();
                sweetAlertDialog.setTitle("Registrando cuenta");
                sweetAlertDialog.setTitleText("Cargando...");
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.show();


                carrera.setConductor_id(preferencia.getConductorId());
                carrera.setEstado("Finalizado");
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                carrera.setFin(formatter.format(date));
                db.collection("Carreras").
                        // below line is use toset the id of
                        // document where we have to perform
                        // update operation.
                                document(preferencia.getCarreraId()).

                        // after setting our document id we are
                        // passing our whole object class to it.
                                set(carrera).

                        // after passing our object class we are
                        // calling a method for on success listener.
                                addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                sweetAlertDialog.dismiss();
                                abrirDialogoError("La carrera se finalizó", "La carrera se finalizó exitosamente.");
                                // on successful completion of this process
                                // we are displaying the toast message.
                                Log.d("RESPUESTA", "correcto");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    // inside on failure method we are
                    // displaying a failure message.
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("RESPUESTA", "fallo");
                        sweetAlertDialog.dismiss();
                    }
                });
                }else{
                    abrirDialogoError("La carrera no se inició", "No hay ninguna carrera en curso.");
                }
            }
        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        db = FirebaseFirestore.getInstance();
        docRef = db.collection("Lineas").document(preferencia.getLineaId());
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Linea linea = documentSnapshot.toObject(Linea.class);

                Log.d("RESPUESTA", "DocumentSnapshot data: " + linea.getLI().get("coordenadas"));
                /*Gson g = new Gson();
                List<Punto> s = g.fromJson(linea.getLI().get("coordenadas").toString(), (Type) Punto.class);*/
                Type listType = new TypeToken<ArrayList<Punto>>() {
                }.getType();
                puntos = new Gson().fromJson(linea.getLI().get("coordenadas").toString(), listType);
                puntosVuelta = new Gson().fromJson(linea.getLV().get("coordenadas").toString(), listType);
                Log.d("RESPUESTA", "DocumentSnapshot data: " + puntos.get(0).getLatitud());
                PolylineOptions options = new PolylineOptions().width(8).color(Color.BLUE).geodesic(true);
                PolylineOptions optionsV = new PolylineOptions().width(8).color(Color.GREEN).geodesic(true);
                for (int z = 0; z < puntos.size(); z++) {
                    LatLng point = new LatLng(puntos.get(z).getLatitud(), puntos.get(z).getLongitud());
                    options.add(point);
                }
                mMap.addPolyline(options);
                for (int z = 0; z < puntosVuelta.size(); z++) {
                    LatLng point = new LatLng(puntosVuelta.get(z).getLatitud(), puntosVuelta.get(z).getLongitud());
                    optionsV.add(point);
                }
                mMap.addPolyline(optionsV);

            }
        });

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        Location location = locationManager.getLastKnownLocation(locationManager
                .getBestProvider(criteria, false));
        double dLatitude = location.getLatitude();
        double dLongitude = location.getLongitude();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 15));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 15));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            LocationManager locationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));
            double dLatitude = location.getLatitude();
            double dLongitude = location.getLongitude();
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 20));
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        Location location = locationManager.getLastKnownLocation(locationManager
                .getBestProvider(criteria, false));
        double dLatitude = location.getLatitude();
        double dLongitude = location.getLongitude();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 20));
    }

    private void getMyLocation() {
        try {
            if (mMap.getMyLocation() != null) { // Compruebe para asegurarse de que las coordenadas no son nulas, probablemente una mejor manera de hacerlo ...
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude()), 15));
            }
        } catch (Exception E) {
        }
    }

    private void ejecutar() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                LocationManager locationManager = (LocationManager)
                        getSystemService(Context.LOCATION_SERVICE);
                Criteria criteria = new Criteria();

                if (ActivityCompat.checkSelfPermission(MapaActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapaActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                Location location = locationManager.getLastKnownLocation(locationManager
                        .getBestProvider(criteria, false));
                double dLatitude = location.getLatitude();
                double dLongitude = location.getLongitude();
                Log.d("RESPUESTA", String.valueOf(dLatitude));
                Recorrido recorrido = new Recorrido();
                recorrido.setConductor_id(preferencia.getConductorId());
                recorrido.setCarrera_id(preferencia.getCarreraId());
                recorrido.setLinea(preferencia.getLinea());
                recorrido.setLatitud(dLatitude);
                recorrido.setLongitud(dLongitude);

                db.collection("Recorridos")
                        .add(recorrido)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                sweetAlertDialog.dismiss();

                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                abrirDialogoError("Lo sentimos, ha ocurrido un error", "Inténtelo más tarde, por favor.");
                                sweetAlertDialog.dismiss();
                            }
                        });
                handler.postDelayed(this,30000);//se ejecutara cada 10 segundos
            }
        },5000);//empezara a ejecutarse después de 5 milisegundos
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(MapaActivity.this,LoginActivity.class);
        startActivity(i);
        finish();
    }

    private void checkLocationSetting(LocationSettingsRequest.Builder builder) {

        builder.setAlwaysShow(true);

        // Dentro de la variable 'cliente' iniciamos LocationServices, para los servicios de ubicación
        SettingsClient cliente = LocationServices.getSettingsClient(this);

        // Creamos una task o tarea para verificar la configuración de ubicación del usuario
        Task<LocationSettingsResponse> task = cliente.checkLocationSettings(builder.build());

        // Adjuntamos OnSuccessListener a la task o tarea
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                // Si la configuración de ubicación es correcta,
                // se puede iniciar solicitudes de ubicación del usuario
                // mediante el método iniciarActualizacionesUbicacion() que crearé más abajo.
                iniciarActualizacionesUbicacion();

            }
        });

        // Adjuntamos addOnCompleteListener a la task para gestionar si la tarea se realiza correctamente
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {

            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // En try podemos hacer 'algo', si la configuración de ubicación es correcta,

                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            // La configuración de ubicación no está satisfecha.
                            // Le mostramos al usuario un diálogo de confirmación de uso de GPS.
                            try {
                                // Transmitimos a una excepción resoluble.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;

                                // Mostramos el diálogo llamando a startResolutionForResult()
                                // y es verificado el resultado en el método onActivityResult().
                                resolvable.startResolutionForResult(
                                        MapaActivity.this,
                                        REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignora el error.
                            } catch (ClassCastException e) {
                                // Ignorar, aca podría ser un error imposible.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // La configuración de ubicación no está satisfecha
                            // podemos hacer algo.
                            break;
                    }
                }
            }
        });

    }
    public void iniciarActualizacionesUbicacion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                return;
            }
        }

        // Obtenemos la ubicación más reciente
        fusedLocationClient.requestLocationUpdates(mLocationRequest,
                mlocationCallback,
                null /* Looper */);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                // Se cumplen todas las configuraciones de ubicación.
                // La aplicación envía solicitudes de ubicación del usuario.
                iniciarActualizacionesUbicacion();
            } else {
                checkLocationSetting(builder);
            }
        }
    }
    private void dialogoSolicitarPermisoGPS(){
        if (ActivityCompat.checkSelfPermission(MapaActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(MapaActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapaActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        }
    }
    private void obtenerUltimaUbicacion() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions

                // muestra una ventana o Dialog en donde el usuario debe
                // dar permisos para el uso del GPS de su dispositivo.
                // El método dialogoSolicitarPermisoGPS() lo crearemos más adelante.
                dialogoSolicitarPermisoGPS();

            }
        }
    }
    private void abrirDialogoError(String titulo,String mensaje) {
        new DialogoNormal.Builder(MapaActivity.this)
                .setTitle(titulo)
                .setMessage(mensaje)
                .setPositiveBtnText("Aceptar")
                .setAnimation(Animacion.POP)
                .isCancellable(false)
                .OnPositiveClicked(new DialogoNormalInterface() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }
}