package com.uagrm.busesconductor.pojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Linea implements Serializable {
    private String name;
    private int id;
    private HashMap<String, Object> LI;
    private HashMap<String, Object> LV;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashMap<String, Object> getLI() {
        return LI;
    }

    public void setLI(HashMap<String, Object> LI) {
        this.LI = LI;
    }

    public HashMap<String, Object> getLV() {
        return LV;
    }

    public void setLV(HashMap<String, Object> LV) {
        this.LV = LV;
    }
}
